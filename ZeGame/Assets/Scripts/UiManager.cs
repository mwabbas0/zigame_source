﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiManager : MonoBehaviour {
     
    public static UiManager instance;
    public GameObject ZegamePanel;
    public GameObject gameOverPanel;
    public GameObject taptext;
    public Text score;
    public Text highScore1;
    public Text highScore2;

    public void Awake(){
        if(instance == null){
            instance = this;
        }
    }

    // Use this for initialization
    void Start () {
        highScore1.text = "High Score: " + PlayerPrefs.GetInt("highScore").ToString();

    }
    public void GameStart() {
        taptext.SetActive(false);
        ZegamePanel.GetComponent<Animator>().Play("panelUp");
    }
	
    public void Gameover(){
        score.text = PlayerPrefs.GetInt("highScore").ToString();
        highScore2.text = PlayerPrefs.GetInt("score").ToString();
        gameOverPanel.SetActive(true);
    }

    public void Reset(){
        SceneManager.LoadScene(0);
    }
    // Update is called once per frame
    void Update () {
		
	}
}
